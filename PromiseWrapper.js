var PromiseWrapper = function(funcs){
    this.i = 0;          // function index
    this.funcs = funcs;  // functions to run
    this.promise = null; // current step that is being done

    this._data = null

    // var for when wrapper is done
    this.done_flag = false;
    this.done_func = null;
}


PromiseWrapper.prototype.next = function(callback){
    var th = this;

    // nothing more to do
    if(this.i > this.funcs.length){
        return this
    }

    // if wrapper hasnt startet
    if(this.i == 0 && this.funcs.length > 0){
        var x = this.i++



        this.promise = new Promise(function(res){

            var resolve = (callback) ?
                    function(){
                        callback(th);
                        res.apply(null, arguments);
                    } : res;

            th.funcs[x](resolve, th._data);
        });
    }

    // do next function
    else{
        var x = th.i++
        this.promise = this.promise.then(function(data){
            return new Promise(function(res){
                var resolve = (callback) ?
                    function(){
                        callback(th);
                        res.apply(null, arguments);
                    } : res ;
                th.funcs[x](resolve, data)
            });
        });
    }

    // if wrapper is done
    if(this.i == this.funcs.length){
        this.i++;
        // concat new promise that runs done function
        this.promise = this.promise.then(function(data){

            return new Promise(function(){
                callback(th);
                th.done_flag = true;
                if(th.done_func){
                    th.done_func(data)
                }
            })
        });
    }

    return this
}

PromiseWrapper.prototype.done = function(done_func){
    this.done_func = done_func;

    if(this.done_flag){
        this.done_func();
    }

    return this
}

PromiseWrapper.prototype.data = function(data){
    this._data = data
    return this
}
