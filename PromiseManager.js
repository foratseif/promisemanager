var PromiseManager = function(data){
    this.data = data

    this.ques = [];
    this.funcs = [];

    this.index = 0;

    this.done_func = null;
    this.last_wrapper = null;
}

PromiseManager.prototype.que = function(batch, func){

    this.ques.push(
        new PromiseQue(batch)
    );

    this.funcs.push(func)

    return this
}

PromiseManager.prototype.done = function(done_func){
    this.done_func = done_func

    // if last wrapper already created
    if(this.last_wrapper){
        this.last_wrapper.done(this.done_func);
    }

    return this
}

PromiseManager.prototype.start = function(){

    // connecting ques
    for(let i=0; i<this.ques.length-1; i++){
        let curr = this.ques[i]
        let next = this.ques[i+1]
        curr.next_que(next);
    }

    // connecting first que to manager
    this.ques[0].prev = this;

    // fill batch of fist que
    let first_batch = this.ques[0].batch

    let min = (first_batch < this.data.length) ? first_batch : this.data.length;

    this.send_finished_to_next();

    return this
}

PromiseManager.prototype.send_finished_to_next = function(){

    for(var i=0; i<this.ques[0].available && this.index < this.data.length; i++){

        let wrapper = this.create_wrapper(this.data[this.index])

        if(this.index == this.data.length-1){
            if(this.done_func){
                wrapper.done(this.done_func);
            }else{
                this.last_wrapper = wrapper
            }
        }

        this.ques[0].add(wrapper);

        this.index++
    }

}


PromiseManager.prototype.create_wrapper = function(data){
    return new PromiseWrapper(this.funcs).data(data)
}
