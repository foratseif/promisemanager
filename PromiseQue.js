// define PromiseQue class
var PromiseQue = function(batch){
    // batch size
    this.batch = batch
    this.available = batch

    this.next = null
    this.prev = null

    if(this.next_que){
        this.next_que.prev_que = this
    }

    // arrays of promise wrappers
    this.finished = []
    this.running = []
}

// add function
PromiseQue.prototype.add = function(wrap){
    // callback on next call
    var th = this;

    this.running.push(wrap)

    wrap.next(function(){
        th._wrap_done(wrap)
    });

    this.available--;
}

// bind to next que
PromiseQue.prototype.next_que = function(que){
    // next que
    this.next = que
    this.next.prev = this
}

PromiseQue.prototype._wrap_done = function(wrap){
    var index = this.running.indexOf(wrap)
    if(index == -1){
        return;
    }

    this.running.splice(index, 1);

    if(this.next == null){
        this.available++;
    }else{
        this.finished.push(wrap);
        this.send_finished_to_next();
    }

    this.prev.send_finished_to_next();
}

PromiseQue.prototype.send_finished_to_next = function(){
    if(!this.finished.length) return;
    if(!this.next.available)  return;

    while(this.next.available && this.finished.length){
        var wrap = this.finished.shift();
        this.next.add(wrap);
        this.available++;

        this.prev.send_finished_to_next();
    }
}
